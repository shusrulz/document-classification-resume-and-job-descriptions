import numpy as np
import pickle
from keras.preprocessing.sequence import pad_sequences # for padding the text sequence to same length
import spacy
from datareader import prepare_text

content = prepare_text("/home/shushant/Downloads/cv.docx",dolower = False)
model_path = '/home/shushant/Desktop/data_generator/document_classification_model.sav'
tokenizer_path = 'tokenizer.pickle'

def categorize_document(content,model_path,tokenizer_path):

	# load saved model
	loaded_model = pickle.load(open(model_path, 'rb'))
	# loading tokenizer
	with open(tokenizer_path, 'rb') as handle:
	    tokenizer = pickle.load(handle)

	label_mapping = {"0":"Job Description","1":"Other Files","2":"Resume"}
	input_text=tokenizer.texts_to_sequences([content])
	padded_text = pad_sequences([input_text[0]], maxlen=750)
	print('Prediction from model {}'.format(loaded_model.predict(padded_text)))
	pred = np.argmax(loaded_model.predict(padded_text))
	category = label_mapping.get(str(pred))
	print("The testing file is categorized as ----> {}".format(label_mapping.get(str(pred))))
	return category

document_category = categorize_document(content,model_path,tokenizer_path)

# nlp = spacy.load("en_core_web_sm")

# # Define the custom component
# def category_component(doc):
#     document_category = categorize_document(content,model_path,tokenizer_path)
#     print("Document is ",document_category)
#     return doc

# nlp.add_pipe(category_component, first=True)

# doc = nlp(content)

